package com.manyiack.sword;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ToolMaterials;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class SwordMod implements ModInitializer{
    public static final String MODID = "manyiack";

    public static final ManyiackSword IRON_SWORD = new ManyiackSword(ToolMaterials.IRON, new Item.Settings().stackSize(1).itemGroup(ItemGroup.COMBAT));

    @Override
    public void onInitialize() {
        Registry.register(Registry.ITEM, new Identifier(MODID, "club"), IRON_SWORD);
    }
    
}